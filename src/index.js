import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import {BrowserRouter,Route} from 'react-router-dom';
import {createStore, applyMiddleware} from 'redux';
import {Provider} from 'react-redux';
import Store from './Store';

// const Root = ({ Store }) => (
//   <Provider store={Store}>
//     <BrowserRouter>
//       <App/>
//     </BrowserRouter>
//   </Provider>
// )

//ReactDOM.render(<Provider store={Store}><BrowserRouter><App /></BrowserRouter></Provider>, document.getElementById('root'));
ReactDOM.render(<Provider store={Store}><App /></Provider>, document.getElementById('root'));
//ReactDOM.render(<BrowserRouter><App /></BrowserRouter>, document.getElementById('root'));
//ReactDOM.render(Root, document.getElementById('root'));


// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
